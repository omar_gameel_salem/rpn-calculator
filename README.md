### RPN Calculator ###

### Requirements to build and run the solution

* Java 8
* Maven

###Tests
```
cd rpn.calculator
mvn clean surefire-report:report
```
report will be at /target/site/surefire-report.html

###Run
```
cd rpn.calculator
mvn clean package
java -jar target/rpn.calculator-1.0-SNAPSHOT.jar
```