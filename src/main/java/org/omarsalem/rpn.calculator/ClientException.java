package org.omarsalem.rpn.calculator;

public class ClientException extends RuntimeException {
    public ClientException(String msg) {
        super(msg);
    }
}
