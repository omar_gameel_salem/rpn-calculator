package org.omarsalem.rpn.calculator;

import org.omarsalem.rpn.calculator.services.Calculator;

import java.util.Scanner;

public class Application {
    private static GlobalExceptionHandler globalExceptionHandler = new GlobalExceptionHandler();
    private static Scanner scanner = new Scanner(System.in);
    private static Calculator calculator = new Calculator();

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(globalExceptionHandler);
        while (true) {
            String input = scanner.nextLine();
            final String output = calculator.enter(input);
            System.out.println(output);
        }
    }
}
