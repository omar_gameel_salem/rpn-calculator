package org.omarsalem.rpn.calculator;

public class GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(Thread t, Throwable e) {
        if (e instanceof ClientException) {
            System.out.println(e.getMessage());
        }
        else {
            System.out.println("something went wrong");
        }
    }
}
