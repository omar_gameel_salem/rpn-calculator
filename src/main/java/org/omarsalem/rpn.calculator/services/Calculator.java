package org.omarsalem.rpn.calculator.services;

import org.apache.commons.lang3.StringUtils;
import org.omarsalem.rpn.calculator.ClientException;
import org.omarsalem.rpn.calculator.InsufficientParametersException;
import org.omarsalem.rpn.calculator.services.concrete.AddCommand;
import org.omarsalem.rpn.calculator.services.concrete.ClearCommand;
import org.omarsalem.rpn.calculator.services.concrete.DivideCommand;
import org.omarsalem.rpn.calculator.services.concrete.MultiplyCommand;
import org.omarsalem.rpn.calculator.services.concrete.SqrtCommand;
import org.omarsalem.rpn.calculator.services.concrete.SubtractCommand;
import org.omarsalem.rpn.calculator.services.concrete.UndoCommand;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

public class Calculator {
    private final Stack<Double> numbers = new Stack<>();
    private final Stack<List<Double>> operations = new Stack<>();

    private final Set<Command> commands = new HashSet<>();
    private final DecimalFormat decimalFormatter = new DecimalFormat("0.##########");

    public Calculator() {
        commands.add(new AddCommand());
        commands.add(new ClearCommand());
        commands.add(new DivideCommand());
        commands.add(new MultiplyCommand());
        commands.add(new SqrtCommand());
        commands.add(new SubtractCommand());
        commands.add(new UndoCommand());

        decimalFormatter.setRoundingMode(RoundingMode.DOWN);
    }

    public String enter(String input) {
        validate(input);
        String error = "";
        final String[] arr = input.split(" ");

        for (int i = 0; i < arr.length; i++) {
            String p = arr[i];
            if (StringUtils.isNumeric(p)) {
                numbers.push(new Double(p));
            } else {
                final Command command = commands
                        .stream()
                        .filter(c -> c.canHandle(p)).findAny()
                        .orElseThrow(() -> new ClientException("invalid operation"));
                try {
                    command.execute(numbers, operations);
                } catch (InsufficientParametersException ipe) {
                    error = String.format("operator %s (position: %s): insufficient parameters\n", command.getCommand(), i * 2 + 1);
                    break;
                }
            }
        }
        final String output = numbers.stream().map(r -> decimalFormatter.format(r)).collect(Collectors.joining(" "));
        return error + "stack: " + output;
    }

    private void validate(String input) {
        if (StringUtils.isBlank(input)) {
            throw new ClientException("empty input");
        }
    }
}
