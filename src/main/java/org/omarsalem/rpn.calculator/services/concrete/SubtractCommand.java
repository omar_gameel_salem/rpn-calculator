package org.omarsalem.rpn.calculator.services.concrete;

public class SubtractCommand extends TwoOperandCommand {

    @Override
    protected String getCommand() {
        return "-";
    }

    @Override
    protected Double operate(Double firstOperand, Double secondOperand) {
        return firstOperand - secondOperand;
    }

    @Override
    public String toString() {
        return "SubtractCommand";
    }
}
