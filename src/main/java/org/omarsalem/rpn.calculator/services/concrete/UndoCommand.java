package org.omarsalem.rpn.calculator.services.concrete;

import org.omarsalem.rpn.calculator.services.Command;

import java.util.List;
import java.util.Stack;

public class UndoCommand extends Command {


    @Override
    protected String getCommand() {
        return "undo";
    }

    @Override
    protected void execute(Stack<Double> numbers, Stack<List<Double>> operations) {
        numbers.pop();
        if (operations.isEmpty()) {
            return;
        }
        operations.pop().forEach(o -> numbers.push(o));
    }

    @Override
    public String toString() {
        return "UndoCommand";
    }
}
