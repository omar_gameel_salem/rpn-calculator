package org.omarsalem.rpn.calculator.services.concrete;

import org.omarsalem.rpn.calculator.InsufficientParametersException;
import org.omarsalem.rpn.calculator.services.Command;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class SqrtCommand extends Command {

    @Override
    protected String getCommand() {
        return "sqrt";
    }

    @Override
    protected void execute(Stack<Double> numbers, Stack<List<Double>> operations) {
        if (numbers.isEmpty()) {
            throw new InsufficientParametersException();
        }
        final Double firstOperand = numbers.pop();
        final Double result = Math.sqrt(firstOperand);
        numbers.push(result);
        operations.add(Arrays.asList(firstOperand));
    }

    @Override
    public String toString() {
        return "SqrtCommand";
    }
}
