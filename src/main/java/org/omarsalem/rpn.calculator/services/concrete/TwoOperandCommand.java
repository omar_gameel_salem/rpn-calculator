package org.omarsalem.rpn.calculator.services.concrete;

import org.omarsalem.rpn.calculator.InsufficientParametersException;
import org.omarsalem.rpn.calculator.services.Command;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public abstract class TwoOperandCommand extends Command {
    @Override
    protected void execute(Stack<Double> numbers, Stack<List<Double>> operations) {
        if (numbers.size() < 2) {
            throw new InsufficientParametersException();
        }
        final Double secondOperand = numbers.pop();
        final Double firstOperand = numbers.pop();
        final Double result = operate(firstOperand, secondOperand);
        numbers.push(result);
        operations.add(Arrays.asList(firstOperand, secondOperand));
    }

    protected abstract Double operate(Double firstOperand, Double secondOperand);
}
