package org.omarsalem.rpn.calculator.services.concrete;

import org.omarsalem.rpn.calculator.ClientException;

public class DivideCommand extends TwoOperandCommand {


    @Override
    protected String getCommand() {
        return "/";
    }

    @Override
    protected Double operate(Double firstOperand, Double secondOperand) {
        if (secondOperand == 0) {
            throw new ClientException("cannot divide by zero");
        }
        return firstOperand / secondOperand;
    }

    @Override
    public String toString() {
        return "DivideCommand";
    }
}
