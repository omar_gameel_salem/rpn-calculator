package org.omarsalem.rpn.calculator.services.concrete;

import org.omarsalem.rpn.calculator.services.Command;

import java.util.List;
import java.util.Stack;

public class ClearCommand extends Command {


    @Override
    protected String getCommand() {
        return "clear";
    }

    @Override
    protected void execute(Stack<Double> numbers, Stack<List<Double>> operations) {
        numbers.clear();
        operations.clear();
    }

    @Override
    public String toString() {
        return "ClearCommand";
    }
}
