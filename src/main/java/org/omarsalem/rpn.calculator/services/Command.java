package org.omarsalem.rpn.calculator.services;


import java.util.List;
import java.util.Stack;

public abstract class Command {
    public boolean canHandle(String operation) {
        return operation.equalsIgnoreCase(getCommand());
    }

    protected abstract String getCommand();

    protected abstract void execute(Stack<Double> numbers, Stack<List<Double>> operations);
}
