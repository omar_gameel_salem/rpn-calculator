package org.omarsalem.rpn.calculator.services;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    private Calculator target;

    @Before
    public void init() {
        target = new Calculator();
    }

    @Test
    public void example1() {
        //Arrange
        String input = "5 2";

        //Act
        final String output = target.enter(input);

        //Assert
        assertEquals("stack: " + input, output);
    }

    @Test
    public void example2() {
        //Arrange
        String input = "2 sqrt";

        //Act
        String output = target.enter(input);

        //Assert
        assertEquals("stack: 1.4142135623", output);

        //Arrange
        input = "clear 9 sqrt";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: 3", output);
    }

    @Test
    public void example3() {
        //Arrange
        String input = "5 2 -";

        //Act
        String output = target.enter(input);

        //Assert
        assertEquals("stack: 3", output);

        //Arrange
        input = "3 -";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: 0", output);

        //Arrange
        input = "clear";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: ", output);
    }

    @Test
    public void example4() {
        //Arrange
        String input = "5 4 3 2";

        //Act
        String output = target.enter(input);

        //Assert
        assertEquals("stack: " + input, output);

        //Arrange
        input = "undo undo *";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: 20", output);

        //Arrange
        input = "5 *";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: 100", output);

        //Arrange
        input = "undo";

        //Act
        output = target.enter(input);

        //Assert
        assertEquals("stack: 20 5", output);
    }

    @Test
    public void example5() {
        //Arrange
        String input = "7 12 2 /";

        //Act
        String output = target.enter(input);

        //Assert
        assertEquals("stack: 7 6", output);

        //Act
        output = target.enter("*");
        assertEquals("stack: 42", output);

        //Act
        output = target.enter("4 /");

        //Assert
        assertEquals("stack: 10.5", output);
    }

    @Test
    public void example6() {
        //Arrange
        String input = "1 2 3 4 5";

        //Act
        target.enter(input);
        String output = target.enter("*");

        //Assert
        assertEquals("stack: 1 2 3 20", output);

        //Act
        output = target.enter("clear 3 4 -");

        //Assert
        assertEquals("stack: -1", output);
    }

    @Test
    public void example7() {
        //Arrange
        String input = "1 2 3 4 5";

        //Act
        target.enter(input);
        String output = target.enter("* * * *");

        //Assert
        assertEquals("stack: 120", output);
    }

    @Test
    public void example8() {
        //Arrange
        String input = "1 2 3 * 5 + * * 6 5";

        //Act
        String output = target.enter(input);

        //Assert
        assertEquals("operator * (position: 15): insufficient parameters\nstack: 11", output);
    }
}