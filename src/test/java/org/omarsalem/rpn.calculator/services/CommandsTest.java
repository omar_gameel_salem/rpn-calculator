package org.omarsalem.rpn.calculator.services;

import org.junit.Before;
import org.junit.Test;
import org.omarsalem.rpn.calculator.ClientException;
import org.omarsalem.rpn.calculator.services.concrete.AddCommand;
import org.omarsalem.rpn.calculator.services.concrete.DivideCommand;
import org.omarsalem.rpn.calculator.services.concrete.MultiplyCommand;
import org.omarsalem.rpn.calculator.services.concrete.SqrtCommand;
import org.omarsalem.rpn.calculator.services.concrete.SubtractCommand;

import java.util.List;
import java.util.Stack;

import static org.junit.Assert.assertEquals;

public class CommandsTest {

    private Stack<Double> numbers;
    private Stack<List<Double>> operations;

    @Before
    public void init() {
        operations = new Stack<>();
        numbers = new Stack<>();
        numbers.add(3d);
        numbers.add(2d);
    }

    @Test
    public void add() {
        //Arrange
        Command add = new AddCommand();

        //Act
        add.execute(numbers, operations);

        //Assert
        assertEquals(Double.valueOf(5), numbers.pop());
    }

    @Test
    public void divide() {
        //Arrange
        Command divide = new DivideCommand();

        //Act
        divide.execute(numbers, operations);

        //Assert
        assertEquals(Double.valueOf(1.5), numbers.pop());
    }

    @Test(expected = ClientException.class)
    public void divideByZero() {
        //Arrange
        Command divide = new DivideCommand();
        final Stack<Double> numbers = new Stack<>();
        numbers.add(3d);
        numbers.add(0d);

        //Act
        divide.execute(numbers, new Stack<>());
    }

    @Test
    public void multiply() {
        //Arrange
        Command multiply = new MultiplyCommand();

        //Act
        multiply.execute(numbers, operations);

        //Assert
        assertEquals(Double.valueOf(6), numbers.pop());
    }

    @Test
    public void sqrt() {
        //Arrange
        Command sqrt = new SqrtCommand();
        numbers = new Stack<>();
        numbers.push(9d);

        //Act
        sqrt.execute(numbers, operations);

        //Assert
        assertEquals(Double.valueOf(3), numbers.pop());
    }

    @Test
    public void subtract() {
        //Arrange
        Command subtract = new SubtractCommand();

        //Act
        subtract.execute(numbers, operations);

        //Assert
        assertEquals(Double.valueOf(1), numbers.pop());
    }
}
